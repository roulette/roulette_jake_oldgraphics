"""this compares the contents of the subdirectories of two directories, namely, prints out all files in dir1 that are
not in dir2. This only compares files that are in subdirectories that exist in both dir1 and dir2"""
import os
def getext(fn):
    x = len(fn)
    for i in range(1,x+1):
        if fn[x-i] == '.':
            return fn[x-i+1:x]

dir1 ="/home/user/stalls/kitchen/faucet01/roulette/src/data/jake_FINAL/clips_old/"
dir2 = "/media/windows/stuff/kitchen/faucet01/posterizeme/jake_FINAL/clips/"

cdir1 = os.listdir(dir1)
cdir2 = os.listdir(dir2)

for e in cdir1:
    if e in cdir2:
        c1 = os.listdir(dir1+e)
        c2 = os.listdir(dir2+e)
        remove = list()
        for f in c1:
            if f in c2:
                remove.append(f)
        for f in remove:
            c1.remove(f)
            c2.remove(f)
        if len(c1) > 0:
            print e
    else:
        print "folder not found", e
